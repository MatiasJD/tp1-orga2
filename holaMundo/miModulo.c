#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init hola_mundo_init(void)
{/* Constructor */
    printk(KERN_INFO "Dutra M: Driver registrado\n");
    return 0;
}

static void __exit hola_mundo_exit(void)
{/* Destructor */
    printk(KERN_INFO "Dutra M: Driver desregistrado\n");
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Matias Dutra");
MODULE_DESCRIPTION("Un primer drive");

module_init(hola_mundo_init);
module_exit(hola_mundo_exit);
